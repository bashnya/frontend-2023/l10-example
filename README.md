# БАШНЯ. Курс по фронту. Код к занятию 10

### На занятии мы разбирали
* Создание бэкэнд-сервера на Node.js
* Библиотеку для создания Web API &mdash; Express.js
* Выполнение SQL запросов к PostgreSQL из Node.js

### Полезные ссылки
* Если хочется потыкать БД, PostgreSQL вместе с pgAdmin (GUI, которое я
  использовал) можно взять [отсюда](https://www.postgresql.org/download/).
  Кстати я приложил в репозиторий дамп базы, из которого ее можно легко
  восстановить &mdash; `dump.sql`. Для этого создайте БД, ПКМ -> Restore,
  выберите файл и жмите Restore.
* [Документация Express.js](https://expressjs.com/)
* [Описанный текстом пример создания CRUD на Express.js](https://blog.logrocket.com/crud-rest-api-node-js-express-postgresql/)
* [Про REST и RESTful API](https://habr.com/ru/articles/483202/)
* Поделать HTTP-запросы можно в Postman, скачивайте его [здесь](https://www.postman.com/downloads/)
