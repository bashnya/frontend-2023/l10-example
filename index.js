import express from 'express';
import bodyParser from 'body-parser';

import db from './src/db.js';


// Инстанс сервера Web API
const app = express();
// Настройка парсинга тела входящих вызовов
// Считаем, что тело прилетает в виде валидного JSON
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true,
}));


const USERS_URL = '/users';
const USER_URL = `${USERS_URL}/:id`;

// Ниже - набор стандартных эндпоинтов (ручек) для CRUD-операций

// GET /users - множественное чтение
app.get(USERS_URL, async (request, response) => {
  try {
    // Выполняем действия над БД
    const users = await db.getUsers();
    // Если все ок, возвращаем статус 200 и ответ БД
    response.status(200).json(users);
  } catch (err) {
    // Если ошибка, возвращаем статус 500 и ошибку
    response.status(500).json(err);
  }
});

// GET /users/:id - одиночное чтение
app.get(USER_URL, async (request, response) => {
  try {
    const users = await db.getUser(request.params.id);
    response.status(200).json(users);
  } catch (err) {
    response.status(500).json(err);
  }
});

// DELETE /users/:id - удаление
app.delete(USER_URL, async (request, response) => {
  try {
    const users = await db.deleteUser(request.params.id);
    response.status(200).json(users);
  } catch (err) {
    response.status(500).json(err);
  }
});

// PUT /users/:id - изменение
app.put(USER_URL, async (request, response) => {
  try {
    const users = await db.updateUser(request.params.id, request.body);
    response.status(200).json(users);
  } catch (err) {
    console.log(err);
    response.status(500).json(err);
  }
});

// POST /users - создание
app.post(USERS_URL, async (request, response) => {
  try {
    const user = await db.createUser(request.body);
    response.status(201).json(user);
  } catch (err) {
    console.log(err);
    response.status(500).json(err);
  }
});


const port = 3000;
// Запускаем сервер Web API на указанном порту
app.listen(port, () => console.log('App running on port ' + port));
