import pg from 'pg';

// Интерфейс для общения с СУБД PostgreSQL
// Передаем параметры, указанные при создании БД
const pool = new pg.Pool({
  user: 'postgres',
  host: 'localhost',
  port: 5432,
  database: 'api',
  password: '12345678',
});

// Чтение всех записей из таблицы
async function getUsers() {
  const response = await pool.query('SELECT * FROM users');
  return response.rows;
}

// Чтение записи с заданным значением свойства
async function getUser(id) {
  const response = await pool.query('SELECT * FROM users WHERE id = $1', [id]);
  return response.rows;
}

// Удаление записи с заданным значением свойства
async function deleteUser(id) {
  await pool.query('DELETE FROM users WHERE id = $1', [id]);
  return id;
}

// Создание записи
async function createUser(user) {
  const { name, email } = user;
  const response = await pool.query(
    `INSERT INTO users (name, email) VALUES ($1, $2) RETURNING *`,
    [name, email],
  );
  return response.rows[0];
}

// Изменение свойств записи с заданным значением свойства
async function updateUser(id, user) {
  const { name, email } = user;
  await pool.query(
    'UPDATE users SET name = $1, email = $2 WHERE id = $3',
    [name, email, id],
  );
  return { name, email, id };
}

// Экспорт всех функций в рамках одного объекта
export default {
  getUsers,
  getUser,
  deleteUser,
  createUser,
  updateUser,
};
